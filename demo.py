from __future__ import nested_scopes
from asyncore import file_dispatcher
from distutils.command.build import build
from fileinput import FileInput
import os
from telnetlib import AUTHENTICATION
from tkinter.messagebox import YES
from urllib import response
from flask import request
from googleapiclient.http import MediaFileUpload
import os
from __future__ import nested_scopes
from turtle import title
from Google import Create_Service

##Create Service Instance------------------------------------------------
CLIENT_SECRET_FILE = "service_secrets.json"
API_NAME ="drive"
API_VERSION ="v3"
SCOPES = ["https://www.googleapis.com/auth/drive"]
service_drive = Create_Service(CLIENT_SECRET_FILE,API_NAME,API_VERSION,SCOPES)
print(dir(service_drive))


API_NAME_2 ="sheets"
API_VERSION_2 ="v4"
SCOPES_2 = ["https://www.googleapis.com/auth/spreadsheets"]
service_sheet = Create_Service(CLIENT_SECRET_FILE,API_NAME_2,API_VERSION_2,SCOPES_2)

##Create Folder------------------------------------------------------------
file_metadata = {
    "name" : "ALICTUS",
    "mimeType" : "application/vnd.google-apps.folder"
}
ALICTUS_folder= service_drive.files().create(body=file_metadata).execute()


##Upload file into folder---------------------------------------------------
folder_id = ALICTUS_folder["id"]
file_name = "ALICTUS.xlsx"
mime_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
file_metadatasub = {
    "name":file_name,
    "parents": [folder_id]
}
service_drive.files().create(
    body=file_metadatasub,
    fields="id"
).execute()


##Create alictus raw spreadsheet----------------------------------------------
def uploadExcel(excelFileName):
   file_metadata = {
       'name': excelFileName, 
       'parents': [folder_id], 
       'mimeType': 'application/vnd.google-apps.spreadsheet',
       "copyRequiresWriterPermission": False
    }
   media = MediaFileUpload(excelFileName, mimetype='application/vnd.ms-excel', resumable=True)

   alictus_raw = service_drive.files().create(body=file_metadata, media_body=media, fields='id').execute()
   return alictus_raw
alictus_raw_id = (uploadExcel("ALICTUS.xlsx"))["id"]
print(alictus_raw_id)
spreadsheet_id =alictus_raw_id

##permission grant for spreadsheet for anyone----------------------------------
domain_permission = {
                'type': 'anyone',
                'role': 'writer',
                'allowFileDiscovery': True,
            }

service_drive.permissions().create(fileId=spreadsheet_id, body=domain_permission, fields="id").execute()


##getting sheetid and sheet name-------------------------------------------------
spreadsheetId = spreadsheet_id
result = service_sheet.spreadsheets().get(
    spreadsheetId=spreadsheetId,
    fields="sheets/properties"
).execute()
print(result["sheets"][0]["properties"]["sheetId"])
print(result["sheets"][0]["properties"]["title"])

sheet_id = result["sheets"][0]["properties"]["sheetId"]
worksheet_name=result["sheets"][0]["properties"]["title"]+"!"

## create column title---------------------------------------------------------
cell_range_insert="G1"
values = (
    ("Total Budget","Note"),
)
value_range_body = {
    "majorDimension":"ROWS",
    "values":values
}
service_sheet.spreadsheets().values().update(
    spreadsheetId=spreadsheet_id,
    valueInputOption="USER_ENTERED",
    range=worksheet_name + cell_range_insert,
    body=value_range_body
).execute()

##create calculations-----------------------------------------------------------
request_body ={
  "requests": [
    {
      "repeatCell": {
        "range": {
          "sheetId": sheet_id,
          "startRowIndex": 1,
          "endRowIndex": 14,
          "startColumnIndex": 6,
          "endColumnIndex": 7
        },
        "cell": {
          "userEnteredValue": {
              "formulaValue": "=C2*E2"
          }
        },
        "fields": "userEnteredValue"
      }
    }
  ]
}
service_sheet.spreadsheets().batchUpdate(
spreadsheetId=spreadsheet_id,
body=request_body
).execute()

##Get Row Data Seperately,x+13 may become row lenght. I did not do that here.Also for simplicity, I could just copy--------------------
##original folder and just delete related row until desired campaign remained.----------------------------------------------------------
x=2
for x in range(2,x+13):
    range_name = worksheet_name+"A"+str(x)+":G"+str(x) 
    response = service_sheet.spreadsheets().values().get(
        spreadsheetId=spreadsheet_id, 
        range=range_name
    ).execute()
    response['values']

    flat_list = []
    for sublist in response['values']:
         for item in sublist:
             flat_list.append(item)
    print(flat_list[0])

    ##copy of the main data file-----------------------------------------------------------------------------
    temp_id=service_drive.files().copy(fileId=spreadsheet_id).execute()

    ##give permisson to copy of data file--------------------------------------------------------------------
    domain_permission = {
              'type': 'anyone',
              'role': 'writer',
               'allowFileDiscovery': True,
            }
    service_drive.permissions().create(fileId=temp_id["id"], body=domain_permission, fields="id").execute()


    ##Change the name---------------------------------------------------------------------------------------------
    change_name={
    "name":flat_list[0]
    }
    service_drive.files().update(fileId=temp_id["id"],body=change_name).execute()

    ##empty spreadsheet for update------------------------------------------------------------------------------------
    rangeAll = 'A2:F14'
    body = {}
    service_sheet.spreadsheets( ).values( ).clear(
        spreadsheetId=temp_id["id"],range=rangeAll,
        body=body ).execute( )

    ##update each spreadsheet with appropriate entry-------------------------------------------------------------------
    cell_range_insert="A2"
    values = (response['values'])
    value_range_body = {
        "majorDimension":"ROWS",
        "values":values
    }
    service_sheet.spreadsheets().values().update(
        spreadsheetId=temp_id["id"],
        range=worksheet_name+"A2",
        valueInputOption="USER_ENTERED",
        body=value_range_body
    ).execute()


